let addlistener=(selector,fn,eventname)=>document.querySelector(selector).addEventListener(eventname,fn);

let togglepopup=(toggleval)=>{
    document.querySelectorAll(".popelem").forEach(el=>{toggleval?el.classList.add("show"):el.classList.remove("show")});
    let popup=document.querySelector(".popup");  
    if(toggleval){
       popup.classList.add("openAnimate");
    }
    else popup.classList.remove("openAnimate");
}

let validateForm=()=>{
    if( [...document.querySelectorAll(".forminput")].every(el=>el.value&&el.value.length))document.querySelector(".submitbtn").disabled=false;
    else document.querySelector(".submitbtn").disabled=true;
}

let submitdata=()=>{
    togglepopup(false);
    alert("data for "+document.getElementById("username").value+" submitted!");
}

addlistener(".clickme",togglepopup.bind(null,true),"click");
addlistener(".background",togglepopup.bind(null,false),"click");
addlistener(".closer",togglepopup.bind(null,false),"click");
addlistener("#username",validateForm,"input");
addlistener("#password",validateForm,"input");
addlistener(".submitbtn",submitdata,"click");
document.addEventListener("keyup",(e)=>{
    switch(e.code){
        case "Escape":
            togglepopup(false);
            break;
        case "Enter":
            togglepopup(true);    
    }
})
